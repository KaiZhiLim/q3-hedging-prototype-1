# Customer Segmentation ######
library(readxl)

#Import Business NMI data#
BusinessData<-read_xlsx(file.choose())

#NMIs of Businesses with Smart Meters#
Bus_NMIs<-BusinessData%>%
  filter(metering_class_code=="COMMS4D")%>%
  pull(nmi)

#Segmenting Meter Data to Business or Residential#
IO_Meter$data<-IO_Meter$data%>%
  mutate(
    Type=case_when(
      NMI.id %in% Bus_NMIs ~ "Business",
      TRUE ~ "Residential"
    )
  )%>%
  mutate(Type=as_factor(Type))