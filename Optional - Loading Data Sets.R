## If loading new data sets from csv is required.

## SA_Demand ####
SA_Demand<-read.csv(file.choose(),header=T)
SA_Demand<-SA_Demand%>%
  as_tibble()%>%
  filter(ProfileName=="NSLP",
         ProfileArea=="UMPLP")%>%
  select(SettlementDate,starts_with("Period"))%>%
  pivot_longer(-SettlementDate,
               names_to="Time",
               values_to="Demand")%>%
  mutate(Time=as_factor(Time))
levels(SA_Demand$Time)<-seq(0,23.5,by=0.5)
SA_Demand<-SA_Demand%>%
  mutate(SettlementDate=dmy(SettlementDate))%>%
  mutate(Time=as.numeric(as.character(Time)))%>%
  read.dst%>%
  as_tsibble(index=AEST)

## IO_Meter ######
IO_Meter<-read.Meter()
IO_Exports<-IO_Meter%>%
  read.Demand("E")%>%
  read.dst%>%
  select(-ACDT)%>%
  as_tsibble(index=AEST)

## Read XML Documents ######
file.names<-list.files(file.choose(),
                       full.names = T)
for(i in 1:length(file.names)){
  x<-i
  y<-read_xml(file.names[i])
  CSVData<-xml_find_all(y,"//CSVData")%>%
    as_list%>%
    flatten()
  z<-read.csv(text=CSVData[[1]],header=T)%>%
    as_tibble()
  # Remember to write the file directory ####
  write_csv(z,file=paste0(
    file,
    x,
    ".csv"
  ))
}
remove(file.names,i,x,y,z)
