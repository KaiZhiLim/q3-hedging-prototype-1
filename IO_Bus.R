#IO Smart Meter Businesses ######
# 1. IO Business Meter Data ####
IO_Bus<-IO_Meter$data%>%
  filter(Type=="Business")%>%
  filter(Suffix%>%startsWith("E"))%>%
  unnest(cols=c(data))%>%
  mutate(Time=Time%/%0.5*0.5)%>%
  group_by(SettlementDate,Time)%>%
  summarise(TotalLoad=sum(Demand),
            n.NMI=n_distinct(NMI.id),
            .groups = "drop")%>%
  mutate(Demand=TotalLoad/n.NMI)%>%
  read.dst%>%
  as_tsibble(index = AEST)

IO_Bus.Mar<-IO_Bus%>%
  filter(AEST>=ymd("2021-03-01"))

# 2. Box-Cox ####
lambda.Bus<-IO_Bus.Mar%>%
  features(Demand,features = guerrero)%>%
  pull(lambda_guerrero)
IO_Bus.Mar<-IO_Bus.Mar%>%
  mutate(box.cox=box_cox(Demand,lambda.Bus))
IO_Bus.Mar %>%
  autoplot(box.cox,color="#0072B2")+
  geom_line(aes(y=Demand),color="grey")+
  scale_x_datetime(date_minor_breaks = "1 day")

# 3. STL Decomp ####
dcmp_IO_Bus<-IO_Bus.Mar%>%
  model(STL(box.cox~trend(window=48*5)+
              season(period="day")+
              season(period="week"),
            robust=T))

components(dcmp_IO_Bus)%>%
  autoplot()+
  scale_x_datetime(date_minor_breaks = "1 day")+
  theme_light()

# 4. Detrending and Inverse Box-Cox ####
IO_Bus.Q3<-components(dcmp_IO_Bus)%>%
  mutate(ConstantTrend=mean(trend))%>%
  transmute(SettlementDate=date(AEST),
         Time=hour(AEST)+minute(AEST)/60,
         AEST,
         box.cox=box.cox-trend+ConstantTrend,
         ConstantTrend,
         season_day,
         season_week,
         remainder)%>%
  mutate(Demand=inv_box_cox(box.cox,lambda.Bus))%>%
  mutate(workday=!(
    wday(AEST,label=T) %in% c("Sat","Sun")
  ))
         
IO_Bus.Q3%>%autoplot(Demand)

# 5. Repeating the Data for Q3 ####
IO_Bus.28d<-IO_Bus.Q3%>%
  filter(AEST>=ymd("2021-03-02"))%>%
  as_tibble()

IO_Bus.Q3.rep<-tsibble(
  AEST=ymd_hm("2021-07-01 00:00",
              tz="Australia/Queensland")+
    seq(0,(30*60*(48*92-1)),by=30*60),
  index = AEST)%>%
  bind_cols(
    IO_Bus.28d[
      rep_len(seq_len(
        nrow(IO_Bus.28d)
      ),
      length.out = 92*48),
    ]%>%
      select(-c("AEST","SettlementDate"))
  )%>%
  mutate(SettlementDate=date(AEST))

write.csv(IO_Bus.Q3.rep,"CSV Outputs/IO_Bus.Q3.rep.csv")
