library(fpp3)
library(tidyverse)
load("R Data/AEMO Data.R")
load("R Data/Hedging Prototype 1.R")
source("R Data/IO Official Functions.R")
source("R Data/Bayes Premium Functions.R")

# Module 0: Data Set Setups #######
# Only run if new data sets have to be defined.
SA_Demand.3yr<-SA_Demand%>%
  filter(year(AEST)>=2017)%>%
  update_tsibble(index=AEST)

IO_Exports.Mar<-IO_Exports%>%
  filter(AEST>=ymd("2021-03-01"))

#0.1 Saving AEMO Data.R
save(SA_Demand,IO_Meter,
     IO_Exports, IO_Exports.Mar,
     read.Meter,read.Demand,
     read.dst,
     file="R Data/AEMO Data.R")

#0.2 Saving Outputs 
save(IO_Res.Q3,NSLP_indv.Q3,
     dcmp.ts_IO.Res.Q3,
     IO_Bus.Q3,IO_Bus.Q3.rep,
     file="R Data/Hedging Prototype 1.R")

# Module 1: Preliminary Analysis #######
#Place all graphings and exploratory analysis here.

# 1.1 Autoplots ####
#Autoplot NSLP Demand - 10-years
autoplot(SA_Demand,Demand)

#Autoplot NSLP Demand - 3-years
SA_Demand.3yr%>%
  autoplot(Demand)

# 1.2 Seasonal Plots ####

SA_Demand%>%gg_season(Demand,period="day")+
  labs(y="MW", title="Electricity Demand: Daily")

SA_Demand %>% gg_season(Demand, period = "week",labels = "left") +
  labs(y="MW", title="Electricity Demand: Weekly")

SA_Demand %>% gg_season(Demand, period = "year",labels = "right") +
  labs(y="MW", title="Electricity Demand: Yearly")

# 1.3 Seasonal Subseries Plots ####
SA_Demand %>% 
  index_by(year_month=yearmonth(AEST))%>%
  summarise(Demand=sum(Demand))%>%
  gg_subseries(Demand)

# Module 2: Seasonal Decompositions ######
# Contains Box-Cox Transformations
# Contains STL Decompositions

# 2.1 Box-Cox ####
# NSLP 
lambda.SA <- SA_Demand %>%
  features(Demand, features = guerrero) %>%
  pull(lambda_guerrero)
#Note: Do not try as lambda is >1
#Box-cox may not be appropriate

# IO_Energy
lambda.IO <- IO_Exports.Mar %>%
  features(Demand, features = guerrero) %>%
  pull(lambda_guerrero)
IO_Exports.Mar<-IO_Exports.Mar%>%
  mutate(box.cox=box_cox(Demand,lambda.IO))
IO_Exports.Mar %>%
  autoplot(box_cox(Demand, lambda.IO),color="#0072B2")+
  geom_line(aes(y=Demand),color="grey")+
  scale_x_datetime(date_minor_breaks = "1 day")

# 2.2(a) STL Decompositions: NSLP Decomposition
# Normal STL Decomposition
dcmp_SA<-SA_Demand%>%
  model(
    STL(Demand~season(period="day")+
          season(period="week")+
          season(period="year"),
        robust=T)
  )
dcmp_SA.3yr<-SA_Demand.3yr%>%
  model(
    STL(Demand~season(period="day")+
          season(period="week")+
          season(period="year"),
        robust=T)
  )

# 2.2(b) STL Decompositions: IO_Exports.Mar Decomposition
# Normal STL Decomposition
dcmp_IO<-IO_Exports.Mar%>%
  model(STL(Demand~trend(window=48*4)+
              season(period="day")+
              season(period="week"),
            robust=T))

# Rigid Trendline example
dcmp_IO.rigid<-IO_Exports.Mar%>%
  model(STL(Demand~season(period="day")+
              season(period="week"),
            robust=T))
components(dcmp_IO)%>%
  autoplot()

# Box-Cox STL Decomposition
dcmp_IO.box<-IO_Exports.Mar%>%
  model(STL_box=STL(box_cox(Demand,lambda)~
                      trend(window=48*3)+
                      season(period="day")+
                      season(period="week"),
                    robust=T))

# 2.3 STL Plots ####
# Component Plots
components(dcmp_IO)%>%
  autoplot()+
  labs(y="Demand (kW)")

# Component Plots - with head function
components(dcmp_IO.box)%>%head(48*3)%>%
  autoplot(season_day)+
  scale_x_datetime(date_minor_breaks = "1 day")

# Seasonally adjusted plots 
components(dcmp_IO)%>%as_tsibble()%>%
  autoplot(Demand,color="gray")+
  geom_line(aes(y=season_adjust),color="#0072B2")+
  labs(y="Demand (kW)")

# Module 3: Model Fit ######


# Module 4: Bayes Premiums ####

# Bayes Premium of 
CredTable_IO.Mar<-IO_Exports.Mar%>%
  as_tibble()%>%
  pivot_wider(names_from = SettlementDate,values_from=Demand)%>%
  Cred_fcn()
CredTable_Mar%>%ggplot(aes(x=Time,y=CredPrem))+
  geom_line(lwd=1)+
  theme_classic()+
  scale_y_continuous(limits=c(0,NA))


# Module 5: Dynamic Harmonic Regression ######
# Do not run unless required.
# Dynamic Harmonic Regression

# Workday identification
work_adj <- SA_Demand.3yr %>%
  mutate(
    DOW = wday(SettlementDate, label = TRUE),
    Working_Day =!(DOW %in% c("Sat", "Sun"))
  )

# Fitting ARIMA + Seasonal Harmonic Regression
SA_Demand.3yr.fit<-work_adj%>%
  model(
    ARIMA(Demand ~ PDQ(0, 0, 0) + pdq(d = 0) + 
            Working_Day +
            fourier(period = "day", K = 10) +
            fourier(period = "week", K = 5) +
            fourier(period = "year", K = 3))
  )

# New Forecast Variables
SA_1yr.new<-new_data(SA_Demand.3yr,48*365)%>%
  mutate(DOW = wday(AEST, label = TRUE),
         Working_Day = 
           !(DOW %in% c("Sat", "Sun")))

# Model Forecasting
SA_1yr.fc<-SA_Demand.3yr.fit%>%
  forecast(new_data=SA_1yr.new)

SA_1yr.fc%>%
  head(48*7)%>%
  autoplot(SA_Demand.3yr%>%tail(48*30))
